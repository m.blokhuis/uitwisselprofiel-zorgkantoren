---
title: Organisatorische interoperabiliteit
weight: 2
---
## Aanlevering

De resultaten dienen soms op een peildatum, soms over een kwartaal, soms over een jaar beschikbaar te zijn. Afhankelijk van de interne processen bij de zorgaanbieders zijn deze minimaal 1 dag en maximaal 1 maand na de gevraagde meetperiode (dag, kwartaal, jaar) beschikbaar. Per vraag is bekeken welke peildatum of peilperiode van een kwartaal wordt gehanteerd: zie Semantische laag voor verdere details.

## Doorlevering

Zorgkantoren ontvangen als concessiehouder de antwoorden van gecontracteerde zorgaanbieders op de informatievragen. Specifiek voor de capaciteitsgegevens geldt dat de geanonimiseerde antwoorden van deze uitvraag gecombineerd met wachtlijstgegevens ten behoeve van de ZN Landelijke monitor verpleegzorg worden doorgeleverd aan ZN.

## Momenten van terugkoppeling

De zorgkantoren zullen in gesprek met de zorgaanbieders invullen geven aan de terug te koppelen informatie voor de zorgaanbieders, bijvoorbeeld als spiegelinformatie.

## Looptijd

De looptijd van het uitwisselprofiel is continu doorlopend tot het moment van wijziging.

## Bewaartermijn

De zorgkantoren zullen zelf de antwoorden op vestigings-, organisatie-, gemeentelijk en regionaal niveau voor maximaal 3 jaar bewaren ten behoeve van trendanalyses. Zorgaanbieders hoeven, naast de reeds bij hen bestaande processen en aanpak van dataopslag van gegevens, in verband met de gegevensuitwisseling die volgt uit dit uitwisselprofiel geen aanvullende opslagmaatregelen te nemen.

## Afspraken bij twijfels over de kwaliteit van gegevens

Als een zorgkantoor twijfels heeft over de interpretatie van de gegevens en eventuele twijfel over de kwaliteit van gegevens, wordt tijdens de reguliere gesprekken tussen zorginkoper/ kwaliteitsadviseur van het zorgkantoor en de betreffende zorgaanbieder de juiste context en toelichting bij de cijfers besproken. 

## Afspraken over een eventuele mogelijkheid tot nalevering en correctie

De gegevens kunnen door de zorgaanbieder worden aangepast en het volgend kwartaal verbeterd worden klaargezet voor het zorgkantoor.

## In- en exclusiecriteria

De uitvraag is bedoeld voor organisaties die onder onderstaande inclusiecriteria vallen:

1. Doelgroep

- Tot de doelgroep behoren alle instellingen met een Wlz contract

- Tot de doelgroep behoren alle instellingen binnen een zorgkantoorregio afbakening (per concessiehouder)

- Tot de doelgroep behoren alle instellingen in de verpleeghuissector

2. Profiel (inclusiecriteria)

- De profielen conform [Wet langdurige zorg](https://wetten.overheid.nl/BWBR0036014/2021-05-12#BijlageA) worden geïncludeerd.
