# Indicator: Zorgkantoren 7.1
# Parameters: $(kwartaal)
# Ontologie: versie 2.0.0 of nieuwer

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX onz-zorg: <http://purl.org/ozo/onz-zorg#>

SELECT 
    ?vestiging
    ?ingezette_uren
    ?aantal_clienten
    ((?ingezette_uren/?aantal_clienten) AS ?ingezetten_uren_per_client)
WHERE 
{
    {
        SELECT 
            ?vestiging
            (SUM(?uren) AS ?ingezette_uren)
        WHERE
        {
            # definieer kwartaal waarin overeenkomst geldig moet zijn
            BIND('Q2' AS ?kwartaal)
            BIND(
                IF(?kwartaal = 'Q1', '2023-01-01'^^xsd:date, 
                IF(?kwartaal = 'Q2', '2023-04-01'^^xsd:date,
                IF(?kwartaal = 'Q3', '2023-07-01'^^xsd:date,
                IF(?kwartaal = 'Q4', '2023-10-01'^^xsd:date, 
                '')))) AS ?start_periode)
            BIND((?start_periode + "P3M"^^xsd:duration - "P1D"^^xsd:duration) AS ?eind_periode)

            # selecteer werkovereenkomsten waarbij werknemer een zorgverlenerfunctie heeft
            ?functie
                a onz-pers:ZorgverlenerFunctie ;
                onz-g:startDatum ?start_functie .
            OPTIONAL { ?functie onz-g:eindDatum ?eind_functie .}
            
            FILTER(?start_functie <= ?eind_periode && ((?eind_functie >= ?start_periode) || (!BOUND(?eind_functie))))
            BIND(IF(!BOUND(?eind_functie), ?eind_periode, 
                IF(?eind_functie < ?eind_periode, ?eind_functie, ?eind_periode)
            ) AS ?eind_functie_reken)
            BIND(IF(?start_functie < ?start_periode, ?start_periode, ?start_functie) AS ?start_functie_reken)
            
            ?overeenkomst 
                a onz-pers:ArbeidsOvereenkomst ;
                onz-g:isAbout ?locatie_contract ;
                onz-g:isAbout ?functie .

            # selecteer uren van gewerkte periode binnen rekenperiode
            ?gewerkteperiode 
                a onz-pers:GewerktePeriode ;
                onz-g:hasBeginTimeStamp ?start_werk_datetime ;
                onz-g:hasEndTimeStamp ?eind_werk_datetime ;
                onz-g:definedBy ?overeenkomst ;
                onz-g:hasQuality / onz-g:hasQualityValue / onz-g:hasDataValue ?uren_voor_correctie .
            OPTIONAL{?gewerkteperiode onz-g:hasPerdurantLocation ?locatie_werk .}
            # Als begin- en endTimeStamp van type dateTime zijn dan omzetten zodat vergeleken kan worden met start en eind period
            BIND(STRDT(SUBSTR(STR(?start_werk_datetime), 1, 10), xsd:date) AS ?start_werk)
            BIND(STRDT(SUBSTR(STR(?eind_werk_datetime), 1, 10), xsd:date) AS ?eind_werk)
            FILTER((?start_werk >= ?start_functie_reken) && (?eind_werk <= ?eind_functie_reken))
            
            ?locatie_contract a onz-g:StationaryArtifact
            BIND(IF(!BOUND(?locatie_werk), ?locatie_contract, ?locatie_werk) AS ?locatie)

            # selecteer vestiging van gewerkte periode
            {
                ?locatie onz-g:partOf* ?vestiging_uri .
                ?vestiging_uri 
                    a onz-org:Vestiging ;
                    onz-g:identifiedBy ?vest_nr.
                ?vest_nr a onz-org:Vestigingsnummer ;
                    onz-g:hasDataValue ?vestiging .
            } UNION {
            # includeer ook de organisatie als geheel en label deze als vestiging
                ?locatie onz-g:partOf*/onz-org:vestigingVan ?organisatie_uri .
                ?organisatie_uri 
                    a onz-g:Business ;
                    rdfs:label ?organisatie .
                BIND(CONCAT('Totaal ',?organisatie) AS ?vestiging)
            }

            BIND(IF(!BOUND(?locatie_werk) && BOUND(?vestiging_uri), 0, 1) AS ?correctie)
            BIND(?uren_voor_correctie * ?correctie AS ?uren)
        }
        GROUP BY ?vestiging
    }
    {
        SELECT 
            ?vestiging
            (COUNT(DISTINCT(?client)) AS ?aantal_clienten)
        WHERE
        {
            # definieer kwartaal waarin overeenkomst geldig moet zijn
            BIND('Q2' AS ?kwartaal)
            BIND(
                IF(?kwartaal = 'Q1', '2023-01-01'^^xsd:date, 
                IF(?kwartaal = 'Q2', '2023-04-01'^^xsd:date,
                IF(?kwartaal = 'Q3', '2023-07-01'^^xsd:date,
                IF(?kwartaal = 'Q4', '2023-10-01'^^xsd:date, 
                '')))) AS ?start_periode)
            BIND((?start_periode + "P3M"^^xsd:duration - "P1D"^^xsd:duration) AS ?eind_periode)
            
            VALUES ?zorgprofiel { onz-zorg:4VV onz-zorg:5VV onz-zorg:6VV onz-zorg:7VV onz-zorg:8VV onz-zorg:9BVV onz-zorg:10VV }
            
            # selecteer zorgproces binnen periode
            ?zorgproces
                a onz-zorg:NursingProcess ;
                onz-g:definedBy ?indicatie ;
                onz-g:hasPerdurantLocation ?locatie ;
                onz-g:startDatum ?start_proces .
            OPTIONAL {?zorgproces onz-g:eindDatum ?eind_proces}
            FILTER (?start_proces <= ?eind_periode && (!BOUND(?eind_proces) || ?eind_proces >= ?start_periode))

            # selecteer vestiging van gewerkte periode
            {
                ?locatie onz-g:partOf* ?vestiging_uri .
                ?vestiging_uri 
                    a onz-org:Vestiging ;
                    onz-g:identifiedBy ?vest_nr.
                ?vest_nr a onz-org:Vestigingsnummer ;
                    onz-g:hasDataValue ?vestiging .
            } UNION {
                # includeer ook de organisatie als geheel en label deze als vestiging
                ?locatie onz-g:partOf*/onz-org:vestigingVan ?organisatie_uri .
                ?organisatie_uri 
                    a onz-g:Business ;
                    rdfs:label ?organisatie .
                BIND(CONCAT('Totaal ',?organisatie) AS ?vestiging)
            }

            # selecteer de bijbehordene indicatie, om unieke clienten te kunnen bepalen
            ?indicatie 
                onz-g:hasPart ?zorgprofiel ;
                onz-g:isAbout ?client .
            ?client a onz-g:Human .
        }
    GROUP BY ?vestiging
    }
}
