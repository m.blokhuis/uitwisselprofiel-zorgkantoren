---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie** |  |
|---|---|
| Release | 1.1.2 |
| Versie | 1.1.2 |
| Doel | Release 1.1.2 betreft een uitbreiding van de informatiebehoefte van zorgkantoren naar informatie over capaciteit in ondersteuning van het inkoopproces en ten behoeve van beleidsontwikkeling. |
| Doelgroep | Zorgkantoren (CZ, DSW, Salland, Menzis, VGZ, Zilveren Kruis, Zorg & Zekerheid); Zorgaanbieders verpleeghuiszorg; Zorgverzekeraars Nederland|
| Totstandkoming | De ontwikkeling van release 1.1.2 is uitgevoerd door het programma KIK-V in samenwerking met zorgkantoren DSW, VGZ, Zilveren Kruis, CZ, Menzis, Zorg & Zekerheid evenals Zorgverzekeraars Nederland. De uitwerking is getoetst bij en akkoord bevonden door het andere zorgkantoor Salland. Release 1.1 wordt vastgesteld door de Ketenraad KIK-V. |
| Inwerkingtreding | 15-04-2024 |
| Operationeel toepassingsgebied | Het volgen van contractafspraken tussen zorgkantoor en zorgaanbieders het gebied van capaciteit |
| Status | Ter vaststelling door Ketenraad KIK-V |
| Functionele scope | Release 1.1.2 omvat de aanvulling capaciteitsinformatie ten behoeve van de implementatie |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0). |