# Indicator: Zorgkantoren 4.1 
# Parameters: $(kwartaal)
# Ontologie: versie 2.0.0 of nieuwer

PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX time: <http://www.w3.org/2006/time#>

SELECT 
	?vestiging
	(SUM(?contractuele_uren_kwartaal * ?zorg)/SUM(?dagen_indicator) AS ?zorg_gerelateerd)
	(SUM(?contractuele_uren_kwartaal * ?niet_zorg)/SUM(?dagen_indicator) AS ?niet_zorg_gerelateerd)
WHERE { 
    # Selecteer kwartaal 
    BIND('Q1' AS ?kwartaal)
    BIND(IF(?kwartaal = 'Q1', '2023-01-01'^^xsd:date, 
        IF(?kwartaal = 'Q2', '2023-04-01'^^xsd:date,
        IF(?kwartaal = 'Q3', '2023-07-01'^^xsd:date,
        IF(?kwartaal = 'Q4', '2023-10-01'^^xsd:date, 
        '')))) AS ?start_periode)
    BIND((?start_periode + "P3M"^^xsd:duration - "P1D"^^xsd:duration) AS ?eind_periode)
    
    # Selecteer overeenkomst met start en eind en filter binnen kwartaal
	?overeenkomst 
    	a onz-pers:WerkOvereenkomst ;
        onz-g:hasPart ?omvang ;
        onz-g:isAbout ?locatie .

    # Selecteer contractomvang
    ?omvang
    	a onz-pers:ContractOmvang ;
        onz-g:startDatum ?omvang_start ;
    	onz-g:isAbout ?omvang_waarde .
  	OPTIONAL {?omvang a onz-pers:ContractOmvang ;
                        onz-g:eindDatum ?omvang_eind}
    FILTER (?omvang_start <= ?eind_periode && (!BOUND(?omvang_eind) || ?omvang_eind >= ?start_periode))    

	?omvang_waarde
        onz-g:hasDataValue ?omvang_waarde_getal ;
        onz-g:hasUnitOfMeasure ?omvang_waarde_eenheid .
  	?omvang_waarde_eenheid
    	onz-pers:hasDenominatorQualityValue onz-g:Week ;
     	onz-pers:hasNumeratorQualityValue onz-g:Uur ;
      	onz-g:hasDataValue ?omvang_waarde_factor .
    
    # Bepaal de vestiging waar de werkzaamheden verricht worden volgens de werkovereenkomst
    {
        ?locatie onz-g:partOf* ?vestiging_uri .
        ?vestiging_uri 
            a onz-org:Vestiging ;
            onz-g:identifiedBy ?vest_nr.
        ?vest_nr a onz-org:Vestigingsnummer ;
            onz-g:hasDataValue ?vestiging .
    } UNION {
        # Includeer ook de organisatie als geheel en label deze als vestiging
        ?locatie onz-g:partOf*/onz-org:vestigingVan ?organisatie_uri .
        ?organisatie_uri 
            a onz-g:Business ;
            rdfs:label ?organisatie .
        BIND(CONCAT('Totaal ',?organisatie) AS ?vestiging)
    }
	
    # Definieer of de verloonde uren voor een zorgfunctie zijn
    OPTIONAL {
        ?overeenkomst onz-g:isAbout ?zorgfunctie .
        ?zorgfunctie a onz-pers:ZorgverlenerFunctie
    }
    
	# Maak variabele waarmee of de verloonde uren vermenigvuldigd kunnen worden 
    BIND(IF(BOUND(?zorgfunctie),1,0) AS ?zorg)
	BIND(IF(?zorg=1,0,1) AS ?niet_zorg)
    
    # Bereken aantal uren per week en zet dit om naar kwartaal uren
    BIND((?omvang_waarde_getal * ?omvang_waarde_factor) AS ?uren_per_week)
   	BIND((47*?uren_per_week/4) AS ?contractuele_uren_kwartaal)
    
    # Bepaal aantal dagen overeenkomst in rapportageperiode
    BIND(IF(?omvang_start < ?start_periode, ?start_periode, ?omvang_start) AS ?start_indicator)
    BIND(IF(?omvang_eind > ?eind_periode || !BOUND(?omvang_eind), ?eind_periode, ?omvang_eind) AS ?eind_indicator)

    ?start_indicator ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?start_indicator_final .
    ?eind_indicator ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?eind_indicator_final .
    BIND(?eind_indicator_final - ?start_indicator_final + 1 AS ?dagen_indicator)
} 
GROUP BY ?vestiging
