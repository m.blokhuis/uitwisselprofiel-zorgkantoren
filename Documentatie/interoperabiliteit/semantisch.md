---
title: Semantische interoperabiliteit
weight: 3
---
## Algemeen

| Gegeven | Doel en achtergrond |
| --- | --- |
| AGB code organisatie | Via de AGB code kunnen zorgkantoren declaraties koppelen aan het opgegeven NZA nummer en zo de productieaantallen controleren. |
| NZa-nummer, KvK nummer organisatie, KvK vestigingsnummers, CAK nummer, Naam organisatie, Naam vestigingen | Ter controle juiste zorgaanbieder |
| Contactpersoon zorgaanbieder | Indien er bij het zorgkantoor vragen zijn over hetgeen is aangeleverd, kan het zorgkantoor contact opnemen met dit contactpersoon. |

## Personeel

| **Vraag**                                                                                            |
|------------------------------------------------------------------------------------------------------|
| 1.1 Wat is het gemiddeld aantal personeelsleden?                                                     |
| 1.2 Wat is het aantal personeelsleden op een peildatum?                                              |
| 2.1 Wat is het aantal ingezette uren?                                                                |
| 2.2 Wat is het aantal verloonde uren?                                                                |
| 3.1 Wat is het percentage personeel met een arbeidsovereenkomst voor bepaalde tijd op een peildatum? |
| 4.1 Wat is de gemiddelde contractomvang van het personeel?                                           |
| 5.1 Wat is de leeftijdsopbouw van het personeel?                                                     |
| 6.1 Wat is het percentage ingezette uren personeel per kwalificatieniveau?                           |
| 7.1 Wat is het aantal ingezette uren personeel ingezet per cliënt?                                   |
| 8.1 Wat is het percentage ingezette uren personeel niet in loondienst (PNIL)?                        |
| 8.2 Wat is het percentage kosten personeel niet in loondienst (PNIL)?                                |
| 9.1 Wat is het aantal vrijwilligers?                                                                 |
| 10.1 Wat is het aantal leerlingen?                                                                   |
| 11.1 Wat is het kortdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)?                      |
| 11.2 Wat is het kortdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)?                      |
| 11.3 Wat is het langdurend ziekteverzuimpercentage (excl. zwangerschapsverlof)?                      |
| 11.4 Wat is het langdurend ziekteverzuimpercentage (incl. zwangerschapsverlof)?                      |
| 12.1 Wat is de verzuimfrequentie (excl. zwangerschapsverlof)?                                        |
| 12.2 Wat is de verzuimfrequentie (incl. zwangerschapsverlof)?                                        |
| 13.1 Wat is het percentage instroom personeel?                                                       |
| 13.2 Wat is het percentage uitstroom personeel?                                                      |
| 13.3 Wat is het percentage doorstroom personeel naar oplopend kwalificatieniveau?                    |
| 13.4 Wat is het percentage doorstroom personeel naar aflopend kwalificatieniveau?                    |

**Doel en achtergrond**

1. Zorgkantoren willen inzicht in o.a. personeelsopbouw, verloop en ziekteverzuim, omdat de ontwikkelingen op de arbeidsmarkt van groot belang zijn.
2. Voor zorgkantoren is het van belang om te weten of ze het leveren van zorg kunnen blijven garanderen of dat er actie moet worden ondernomen.
3. Zorgkantoren willen zorgaanbieders met elkaar kunnen vergelijken in een benchmark. Op die manier kunnen zij minder presenterende zorgaanbieders koppelen aan beter presterende zorgaanbieders, zodat zij van elkaar kunnen leren.

## Cliënten

| Vraag |
| --- |
| 14.1 Wat is het aantal cliënten per zorgprofiel? |
| 14.2 Wat is het aantal cliënten per leveringsvorm? |
| 14.3 Wat is het aantal cliënten per zorgprofiel per leveringsvorm? |

**Doel en achtergrond**

1. Deze indicatoren schetsen iets over het profiel van de cliënten die er wonen, hoe deze zorg gefinancierd wordt en hoe zwaar de zorg is (want de indicatie geeft de zorgzwaarte aan).
2. Met en zonder behandeling laat zien hoe de zorg georganiseerd is en waar verantwoordelijkheden liggen (de huisarts of specialist ouderengeneeskunde). Hierin worden vaak grote kwaliteitsverschillen gemerkt en daarom is dit een belangrijk aandachtspunt.

## Capaciteit

| Vraag |
| --- |
| 15.1 Wat is het aantal wooneenheden? |
| 15.2 Wat is het aantal bezette wooneenheden? |
| 15.3 Wat is het aantal personen dat per vestiging kan wonen? |
| 15.4.1 Wat is het aantal bewoners per wet? |
| 15.4.2 Wat is het aantal cliënten per Wlz-indicatie per sector? |
| 15.4.3 Wat is het aantal cliënten per Wlz-VV leveringsvorm? |
| 16.1 Wat is het aantal cliënten met syndroom van Korsakov? |
| 17.1 Wat is de geplande toe- of afname van het aantal wooneenheden? |
| 17.2 Wat is de geplande toe- of afname van het aantal cliënten per (clustering) van leveringsvorm(en)? |

**Doel en achtergrond**

1. De informatievragen over capaciteit geven een overall beeld van de Wlz-capaciteit bij zorgaanbieders en geven een specifieker beeld van de intramurale capaciteit (in wooneenheden of in bewoners) ten opzichte van de ontwikkeling van de geclusterd VPT per vestiging/ organisatie. Voor zorgkantoren is het van belang om zicht te hebben op de beschikbare capaciteit voor cliënten met een Wlz-indicatie, maar ook zicht te hebben op hoe de overige capaciteit bij een zorgaanbieder wordt ingezet. Het is de taak van een zorginkoper om ervoor te zorgen dat ervoor nu en naar de toekomst toe voldoende Wlz-zorg beschikbaar is. 
2. De indicatoren onder hoofdstuk 15 en 16 worden per kwartaal uitgevraagd. De indicatoren onder hoofdstuk 17 wordt per jaar uitgevraagd. De uiterste aanleverdatum voor deze gegevens wordt jaarlijks in samenspraak met de ZN-werkgroep regiomonitor vastgesteld.

## Definities informatievragen

## Algemene uitgangspunten

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel. 

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten:

[Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)


### Indicatoren zorgkantoren

De functionele beschrijving van de berekening per indicator staat hier: [Klik hier](/Gevalideerde_vragen_technisch/)

Voor alle indicatoren Personeel geldt dat deze door de zorgkantoren aanvullend ingedeeld worden per wet (Wlz, Zvw, Wmo, overig) én binnen de Wlz uitgesplitst in langdurige zorgsector (Wlz VV, GGZ-B, GGZ-W, LG, LVG, VG, ZGAUD en ZGVIS). Voor indicator 2.1 Ingezette uren personeel geldt dat deze aanvullend ingedeeld wordt per Wlz VV leveringsvorm (Verblijf, VPT, MPT, DTV, PGB). De zorgkantoren gebruiken de antwoorden op de indicatoren 15.4.1. 15.4.2 en 15.4.3 om deze indelingen te maken.

## Benodigde gegevenselementen

De concepten, eigenschappen en relaties die nodig zijn om de indicatoren zorgkantoren te beantwoorden staan hier: [klik hier](/Gevalideerde_vragen_technisch/Modelgegevensset).

## Aggregatieniveau

Het aggregatieniveau is voor alle indicatoren organisatie- en vestigingsniveau.

## Contextinformatie

In de reguliere gesprekscyclus tussen zorgkantoor en zorgaanbieder kan een toelichting worden gegeven op de uitkomsten in het geval van niet verwachte of afwijkende cijfers. Daarnaast kunnen ook verbeterplannen en kwaliteitsplannen en -verslagen voor contextinformatie worden gebruikt.
