# Indicator: Zorgkantoren 6.1
# Parameters: $(start_periode), $(eind_periode)
# Ontologie: versie 2.0.0 of nieuwer

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT 
    ?vestiging
    ?kwalificatie_niveau
    (?kwalificatie_uren AS ?ingezette_uren)
    ((?kwalificatie_uren/?totaal_uren)*100 AS ?percentage)
WHERE 
{   
    # subquery: bereken het aantal ingezette uren per kwalificatieniveau en vestiging
    {
        SELECT 
            ?vestiging
            ?kwalificatie_niveau
            (SUM(?uren) AS ?kwalificatie_uren)
        WHERE
        {
            # selecteer alle geregistreerde uren van de werknemers per 
            SELECT DISTINCT
                ?gewerkte_periode
                ?start_werk
                ?eind_werk
                ?uren
                ?kwalificatie_niveau
                ?vestiging
            WHERE 
            {      
                BIND ('2023-10-01'^^xsd:date AS ?start_periode)
                BIND ('2023-12-31'^^xsd:date AS ?eind_periode)
                
                # selecteer werkovereenkomsten waarbij werknemer een zorgverlenerfunctie heeft
                ?functie 
                    a onz-pers:ZorgverlenerFunctie ;  
                    a/ onz-g:hasQuality /onz-g:hasQualityValue ?functie_niveau ;
                    onz-g:startDatum ?start_functie .
                OPTIONAL { ?functie onz-g:eindDatum ?eind_functie . }
                
                FILTER(?start_functie <= ?eind_periode && ((?eind_functie >= ?start_periode) || (!BOUND(?eind_functie))))
                BIND(IF(!BOUND(?eind_functie), ?eind_periode, 
                    IF(?eind_functie < ?eind_periode, ?eind_functie, ?eind_periode)
                ) AS ?eind_functie_reken)
                BIND(IF(?start_functie < ?start_periode, ?start_periode, ?start_functie) AS ?start_functie_reken)
                
                ?overeenkomst 
                    a onz-pers:ArbeidsOvereenkomst ;
                    onz-pers:heeftOpdrachtnemer ?medewerker ;
                    onz-g:isAbout ?locatie_contract ;
                    onz-g:isAbout ?functie .

                ?functie_niveau
                    a onz-pers:ODBKwalificatieWaarde ;
                    rdfs:label ?kwalificatie_niveau .

                # selecteer uren van gewerkte periode binnen periode
                ?gewerkte_periode 
                    a onz-pers:GewerktePeriode ;
                    onz-g:definedBy ?overeenkomst ;
                    onz-g:hasBeginTimeStamp ?start_werk_datetime ;
                    onz-g:hasEndTimeStamp ?eind_werk_datetime ; 
                    onz-g:hasQuality / onz-g:hasQualityValue / onz-g:hasDataValue ?uren_voor_correctie .
                OPTIONAL{?gewerkte_periode onz-g:hasPerdurantLocation ?locatie_werk .}
                # Als begin- en endTimeStamp van type dateTime zijn dan omzetten zodat vergeleken kan worden met start en eind period
                BIND(STRDT(SUBSTR(STR(?start_werk_datetime), 1, 10), xsd:date) AS ?start_werk)
                BIND(STRDT(SUBSTR(STR(?eind_werk_datetime), 1, 10), xsd:date) AS ?eind_werk)
                FILTER(?start_werk >= ?start_functie_reken && ?eind_werk <= ?eind_functie_reken)

                ?locatie_contract a onz-g:StationaryArtifact
                BIND(IF(!BOUND(?locatie_werk), ?locatie_contract, ?locatie_werk) AS ?locatie)
                
                # selecteer locatie met bijbehorende vestiging
                {
                    ?locatie onz-g:partOf* ?vestiging_uri .
                    ?vestiging_uri 
                        a onz-org:Vestiging ;
                        onz-g:identifiedBy ?vest_nr.
                    ?vest_nr a onz-org:Vestigingsnummer ;
                        onz-g:hasDataValue ?vestiging .
                } UNION {
                    # Includeer ook de organisatie als geheel en label deze als vestiging
                    ?locatie onz-g:partOf*/onz-org:vestigingVan ?organisatie_uri .
                    ?organisatie_uri
                        a onz-g:Business ;
                        rdfs:label ?Organisatie .
                    BIND(CONCAT('Totaal ',?Organisatie) AS ?vestiging)
                }

                BIND(IF(!BOUND(?locatie_werk) && BOUND(?vestiging_uri), 0, 1) AS ?correctie)
                BIND(?uren_voor_correctie * ?correctie AS ?uren)
            }
        }
        GROUP BY ?vestiging ?kwalificatie_niveau
    }
    # subquery: bereken het aantal ingezette uren per vestiging
    {
        SELECT 
            ?vestiging
            (SUM(?uren) AS ?totaal_uren)
        # selecteer alle geregistreerde uren van de werknemers per vestiging
        WHERE
        {
            SELECT DISTINCT
                ?gewerkte_periode
                ?start_werk
                ?eind_werk
                ?uren
                ?vestiging
            WHERE 
            { 
                BIND ('2023-10-01'^^xsd:date AS ?start_periode)
                BIND ('2023-12-31'^^xsd:date AS ?eind_periode)

                # selecteer werkovereenkomsten waarbij werknemer een zorgverlenerfunctie heeft
                ?functie 
                    a onz-pers:ZorgverlenerFunctie ;  
                    onz-g:startDatum ?start_functie .
                OPTIONAL { ?functie onz-g:eindDatum ?eind_functie . }
                
                FILTER(?start_functie <= ?eind_periode && ((?eind_functie >= ?start_periode) || (!BOUND(?eind_functie))))
                BIND(IF(!BOUND(?eind_functie), ?eind_periode, 
                    IF(?eind_functie < ?eind_periode, ?eind_functie, ?eind_periode)
                ) AS ?eind_functie_reken)
                BIND(IF(?start_functie < ?start_periode, ?start_periode, ?start_functie) AS ?start_functie_reken)
                
                ?overeenkomst 
                    a onz-pers:ArbeidsOvereenkomst ;
                    onz-pers:heeftOpdrachtnemer ?medewerker ;
                    onz-g:isAbout ?locatie_contract ;
                    onz-g:isAbout ?functie .
                
                # selecteer uren van gewerkte periode binnen periode
                ?gewerkte_periode 
                    a onz-pers:GewerktePeriode ;
                    onz-g:definedBy ?overeenkomst ;
                    onz-g:hasBeginTimeStamp ?start_werk_datetime ;
                    onz-g:hasEndTimeStamp ?eind_werk_datetime ; 
                    onz-g:hasQuality / onz-g:hasQualityValue / onz-g:hasDataValue ?uren_voor_correctie .
                OPTIONAL{?gewerkte_periode onz-g:hasPerdurantLocation ?locatie_werk .}
                # Als begin- en endTimeStamp van type dateTime zijn dan omzetten zodat vergeleken kan worden met start en eind period
                BIND(STRDT(SUBSTR(STR(?start_werk_datetime), 1, 10), xsd:date) AS ?start_werk)
                BIND(STRDT(SUBSTR(STR(?eind_werk_datetime), 1, 10), xsd:date) AS ?eind_werk)
                FILTER(?start_werk >= ?start_functie_reken && ?eind_werk <= ?eind_functie_reken)

                ?locatie_contract a onz-g:StationaryArtifact
                BIND(IF(!BOUND(?locatie_werk), ?locatie_contract, ?locatie_werk) AS ?locatie)

                # selecteer locatie met bijbehorende vestiging
                {
                    ?locatie onz-g:partOf* ?vestiging_uri .
                    ?vestiging_uri 
                        a onz-org:Vestiging ;
                        onz-g:identifiedBy ?vest_nr.
                    ?vest_nr a onz-org:Vestigingsnummer ;
                        onz-g:hasDataValue ?vestiging .
                } UNION {
                    # Includeer ook de organisatie als geheel en label deze als vestiging
                    ?locatie onz-g:partOf*/onz-org:vestigingVan ?organisatie_uri .
                    ?organisatie_uri
                        a onz-g:Business ;
                        rdfs:label ?Organisatie .
                    BIND(CONCAT('Totaal ',?Organisatie) AS ?vestiging)
                }

                BIND(IF(!BOUND(?locatie_werk) && BOUND(?vestiging_uri), 0, 1) AS ?correctie)
                BIND(?uren_voor_correctie * ?correctie AS ?uren)
            }
        }
        GROUP BY ?vestiging
    }
}
ORDER BY ?vestiging ?kwalificatie_niveau
