---
title: Algemeen
weight: 2
---
## Doel van de uitvraag

Zorgkantoren kopen periodiek (voor één of meerdere jaren) zorg in bij gecontracteerde Wlz zorgaanbieders. In de overeenkomst die wordt gesloten tussen het zorgkantoor en de zorgaanbieder worden afspraken gemaakt over het volgen van de gemaakte afspraken. Naast de reguliere gegevensstroom van indicatie- en declaratiegegevens worden hiervoor tot op heden gegevens vergaard via openbaar beschikbare bronnen dan wel extra uitvragen bij de gecontracteerde zorgaanbieders. Zorgkantoren monitoren met deze uitvraag naar personele gegevens, cliëntgegevens en capaciteitsgegevens de (dis)continuïteit van zorg per zorgaanbieder, per gemeente en per zorgkantoorregio. Zorgkantoren gebruiken deze gegevens tevens in trendanalyses en ontwikkeling van nieuw beleid.
