# Indicator: Zorgkantoren 2.1.4
# Parameters: -
# Ontologie: versie 2.0.0 of nieuwer

PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT
    ?Vestiging
    (SUM(?inzet_uren) AS ?indicator)
{
    {
        SELECT DISTINCT
            ?locatie
            ?locatie_werk
            ?inzet
            ?gewerkte_tijd
        {
            BIND(('Q4') AS ?kwartaal)
            BIND(IF(?kwartaal = 'Q1', '2023-01-01'^^xsd:date,
                 IF(?kwartaal = 'Q2', '2023-04-01'^^xsd:date,
                 IF(?kwartaal = 'Q3', '2023-07-01'^^xsd:date,
                 IF(?kwartaal = 'Q4', '2023-10-01'^^xsd:date,
                 '')))) AS ?start_periode)
            BIND(?start_periode + "P3M"^^xsd:duration - "P1D"^^xsd:duration AS ?eind_periode)
           
            # Alle zorgverlener functies
            ?functie
                a onz-pers:ZorgverlenerFunctie ;
                onz-g:startDatum ?start_functie .
            OPTIONAL { ?functie onz-g:eindDatum ?eind_functie . }
           
            # Alle zorg verlener functies die vallen binnen de periode of worden hierop aangepast
            FILTER(?start_functie <= ?eind_periode && ((?eind_functie >= ?start_periode) || (!BOUND(?eind_functie))))
            BIND(IF(!BOUND(?eind_functie), ?eind_periode,
                IF(?eind_functie < ?eind_periode, ?eind_functie, ?eind_periode)
            ) AS ?eind_functie_reken)
            BIND(IF(?start_functie < ?start_periode, ?start_periode, ?start_functie) AS ?start_functie_reken)
           
            # zorgverlener functie zijn onderdeel van een contract
            ?overeenkomst
                a onz-pers:ArbeidsOvereenkomst ;
                onz-g:isAbout ?locatie_contract ;
                onz-g:isAbout ?functie .
           
            # Werkperiode van alle zorgverlener functies
            ?inzet
                a onz-pers:GewerktePeriode ;
                onz-g:definedBy ?overeenkomst ;
                onz-g:hasBeginTimeStamp ?start_inzet_datetime ;
                onz-g:hasEndTimeStamp ?eind_inzet_datetime ;
                onz-g:hasQuality ?gewerkte_tijd .
            OPTIONAL{?inzet onz-g:hasPerdurantLocation ?locatie_werk}
            # Als begin- en endTimeStamp van type dateTime zijn dan omzetten zodat vergeleken kan worden met start en eind period
            BIND(STRDT(SUBSTR(STR(?start_inzet_datetime), 1, 10), xsd:date) AS ?start_inzet)
            BIND(STRDT(SUBSTR(STR(?eind_inzet_datetime), 1, 10), xsd:date) AS ?eind_inzet)
            FILTER (?start_inzet >= ?start_functie_reken && ?eind_inzet <= ?eind_functie_reken)

            ?locatie_contract a onz-g:StationaryArtifact .
            BIND(IF(!BOUND(?locatie_werk), ?locatie_contract, ?locatie_werk) AS ?locatie)
        }
    }
    # Bepaal de vestiging waar de werkzaamheden verricht worden volgens de werkovereenkomst
    {
        ?locatie onz-g:partOf* ?vestiging_uri .
        ?vestiging_uri
            a onz-org:Vestiging ;
            onz-g:identifiedBy ?vest_nr.
        ?vest_nr a onz-org:Vestigingsnummer ;
            onz-g:hasDataValue ?Vestiging .
    } UNION {
        # Includeer ook de organisatie als geheel en label deze als vestiging
        ?locatie onz-g:partOf*/onz-org:vestigingVan ?organisatie_uri .
        ?organisatie_uri
            a onz-g:Business ;
            rdfs:label ?Organisatie .
        BIND(CONCAT('Totaal ',?Organisatie) AS ?Vestiging)
    }

    ?gewerkte_tijd
        a onz-pers:GewerkteTijd ;
        onz-g:hasQualityValue/onz-g:hasDataValue ?inzet_uren_voorl .

    BIND(IF(!BOUND(?locatie_werk) && BOUND(?vestiging_uri), 0, 1) AS ?correctie)
    BIND(?inzet_uren_voorl * ?correctie AS ?inzet_uren)
}
GROUP BY ?Vestiging
ORDER BY ?Vestiging